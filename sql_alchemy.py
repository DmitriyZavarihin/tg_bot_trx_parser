from sqlalchemy import Table, Column, Integer, String, Float, MetaData, ForeignKey, create_engine, select, update, LargeBinary
from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy.future import select
from aiohttp import ClientSession
from datetime import datetime
import calendar
import asyncio
import time


engine = create_engine(f"postgresql://cain:1955@localhost/db_parser")


Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


class User(Base):
    __tablename__ = 'all_transactions'
    id = Column(Integer, primary_key=True)
    time = Column(String(42))
    value = Column(Float)
    name_token = Column(String(6))
    balance = Column(Float)
    from_vallet = Column(String(42))
    to_vallet = Column(String(42))
    metod = Column(String(42))
    transaction_id = Column(String(100))
    rate = Column(String(10))
    currency = Column(String(20))
    purpose = Column(String(20))
    own_adress = Column(String(42))
    blockchain = Column(String(42))
    block_timestamp = Column(String(30))
    bot_set = Column(String(100))


    def __init__(self, time, value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, block_timestamp, bot_set):
        self.time = time
        self.value = value
        self.name_token = name_token
        self.balance = balance
        self.from_vallet = from_vallet
        self.to_vallet = to_vallet
        self.metod = metod
        self.transaction_id = transaction_id
        self.rate = rate
        self.currency = currency
        self.purpose = purpose
        self.own_adress = own_adress
        self.blockchain = blockchain
        self.block_timestamp = block_timestamp
        self.bot_set = bot_set


class Settings(Base):
    __tablename__ = 'settings'
    id = Column(Integer, primary_key=True)
    wallet = Column(String(42))
    comment = Column(String(30))
    group = Column(String(42))
    user = Column(String(42))
    telegram_username = Column(String(42))
    name_list = Column(String(42))
    balance = Column(Float)
    cash = Column(Float)
    id_telegram = Column(String(30))
    last_deal = Column(Integer)
    next_ = Column(Integer)


    def __init__(self, wallet, comment, group, user, telegram_username, name_list, balance, cash, id_telegram, last_deal, next_):
        self.wallet = wallet
        self.comment = comment
        self.group = group
        self.user = user
        self.telegram_username = telegram_username
        self.name_list = name_list
        self.balance = balance
        self.cash = cash
        self.id_telegram = id_telegram
        self.last_deal = last_deal
        self.next_ = next_


async def cread_table():
    # Создание таблицы
    Base.metadata.create_all(engine)


async def get_settings():
    # **************** выборка из settings ********************
    session = Session()
    id = session.query(Settings.id).all()
    wallet = session.query(Settings.wallet).all()
    comment = session.query(Settings.comment).all()
    group = session.query(Settings.group).all()
    user = session.query(Settings.user).all()
    telegram_username = session.query(Settings.telegram_username).all()
    name_list = session.query(Settings.name_list).all()
    balance = session.query(Settings.balance).all()
    cash_marina = session.query(Settings.cash).filter(Settings.id == 1).first()[0]
    cash_sity = session.query(Settings.cash).filter(Settings.id == 2).first()[0]
    id_telegram = session.query(Settings.id_telegram).all()
    settings_list = [id, wallet, comment, group, user, telegram_username, name_list, balance, id_telegram]
    session.close()
    return settings_list, cash_marina, cash_sity


async def get_all_transactions():
    # **************** выборка из all_transactions ********************
    session = Session()
    time_my = (calendar.timegm(time.gmtime()))
    time_3h = time_my - 10800
    block_timestamp = session.query(User).filter(User.block_timestamp >= time_3h).filter(User.bot_set == '').all()
    len_message = len(block_timestamp)
    chanel_mes_list = []
    for i in range(len_message):
        name_list = session.query(Settings.name_list).where(Settings.wallet == block_timestamp[i].own_adress).first()
        if block_timestamp[i].metod == "sending":
            t, o = '-', 'OUT'
        else:
            t, o = '+', 'IN'
        if block_timestamp[i].rate == "":
            r = "???"
        else:
            r = block_timestamp[i].rate
        name_list_m = (f"#{name_list[0]}")
        value_m = (f"({t}) {block_timestamp[i].value} usdt {o}")
        time_m = (f"{block_timestamp[i].time}")
        rate_m = (f"Rate: {r}")
        balance_m = (f"Balance: {block_timestamp[i].balance}")
        if block_timestamp[i].blockchain == 'trx':
            link_m1 = (f'<a href="https://tronscan.org/#/transaction/{block_timestamp[i].transaction_id}">LINK</a>')
        elif block_timestamp[i].blockchain == 'eth':
            link_m1 = (f'<a href="https://etherscan.io/tx/{block_timestamp[i].transaction_id}">LINK</a>')
        else:
            link_m1 = (f'<a href="https://bscscan.com/tx/{block_timestamp[i].transaction_id}">LINK</a>')
        id_m = (f"ID: {block_timestamp[i].id}")
        mess = f"{name_list_m}\n{value_m}\n{time_m}\n{rate_m}\n{balance_m}\n{link_m1} {id_m}"
        chanel_mes_list.append(mess)
    print(calendar.timegm(time.gmtime()), len_message)
    session.close()
    return len_message, chanel_mes_list, block_timestamp


async def message_balance():
    # **************** создать сообщение балансы ********************
    settings_list, cash_marina, cash_sity = await get_settings()
    sum_list = []
    name_list = []
    for i in range(len(settings_list[6])):
        h = f"{settings_list[6][i][0]} {' ' * (12 - (len(settings_list[6][i][0])))} = ${('{0:,}'.format(settings_list[7][i][0]).replace(',', ' '))}\n"
        if i == 5:
            name_list.append('=\n')
        name_list.append(h)
        sum_list.append(settings_list[7][i][0])
    tez = round(sum(sum_list), 2)
    total_tez = ('{0:,}'.format(tez).replace(',', ' '))
    tot = cash_sity + cash_marina
    total_aed = ('{0:,}'.format(tot).replace(',', ' '))
    j = f"=\n<strong>TOTAL_TEZ:     = ${total_tez}</strong>"
    #j = f"=\n<strong>TOTAL_TEZ:     = ${total_tez}</strong>\n=\nAED_CITY:         = {('{0:,}'.format(cash_sity).replace(',', ' '))}\nAED_MAR:         = {('{0:,}'.format(cash_marina).replace(',', ' '))}\n<strong>TOTAL_AED:    = {total_aed}</strong>"
    name_list.append(j)
    messag = (''.join(name_list))
    return messag


async def tg_id_get(wall):
    # **************** выборка settings id tg ********************
    session = Session()
    tg_id = session.query(Settings.id_telegram).where(Settings.wallet == wall).first()
    tg_id = tg_id[0]
    session.close()
    return tg_id



async def update_bot_set(id_m_list, id_mes_l):
    # **************** запись в бот сет ********************
    id_mes_l = str(id_mes_l)
    session = Session()
    try:
        session.query(User).filter(User.id == id_m_list).update({User.bot_set: id_mes_l})
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления bot_set в БД")
    session.close()


async def update_rate(id_msql, rate):
    # **************** запись в rate ********************
    rate = str(rate)
    session = Session()
    try:
        session.query(User).filter(User.id == id_msql).update({User.rate: rate})
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления rate в БД")
    session.close()


async def get_id_mes_ls(id_msql):
    # **************** запись в rate ********************
    session = Session()
    try:
        bot_set = session.query(User.bot_set).where(User.id == id_msql).first()
        bot_set = bot_set[0]
        list_rate = []
        h = bot_set.split(',')
        for i in range(3):
            list_rate.append(h[i].split('/'))
        session.close()
        return list_rate
    except:
        session.rollback()
        session.close()
        print("Ошибка запроса id_mes в БД")
        return 0


async def get_alarm_mess():
    # **************** выборка транзакций для напоминания ********************
    session = Session()
    time_my = (calendar.timegm(time.gmtime()))
    time_3h = time_my - 10800
    block_timestamp = session.query(User).filter(User.block_timestamp >= time_3h).filter(User.bot_set != '').filter(User.rate == '').all()
    session.close()
    return block_timestamp


async def get_all_settings(id_setings):
    # **************** выборка settings id tg ********************
    session = Session()
    settings = session.query(Settings).where(Settings.id_telegram == id_setings).all()
    session.close()
    return settings


async def get_end_tr(mess):
    # **************** выборка крайней транзакции для сообщения ********************
    session = Session()
    list_name = mess[1:]
    obj_adress = session.query(Settings.wallet).where(Settings.name_list == list_name).first()
    obj = session.query(User).where(User.own_adress == obj_adress[0]).order_by(User.id.desc()).first()
    if obj.metod == "sending":
        t, o = '-', 'OUT'
    else:
        t, o = '+', 'IN'
    if obj.rate == "":
        r = "???"
    else:
        r = obj.rate
    name_list_m = (f"#{list_name}")
    value_m = (f"({t}) {obj.value} usdt {o}")
    time_m = (f"{obj.time}")
    rate_m = (f"Rate: {r}")
    balance_m = (f"Balance: {obj.balance}")
    if obj.blockchain == 'trx':
        link_m1 = (f'<a href="https://tronscan.org/#/transaction/{obj.transaction_id}">LINK</a>')
    elif obj.blockchain == 'eth':
        link_m1 = (f'<a href="https://etherscan.io/tx/{obj.transaction_id}">LINK</a>')
    else:
        link_m1 = (f'<a href="https://bscscan.com/tx/{obj.transaction_id}">LINK</a>')
    id_m = (f"ID: {obj.id}")
    mess = f"{name_list_m}\n{value_m}\n{time_m}\n{rate_m}\n{balance_m}\n{link_m1} {id_m}"
    session.close()
    id_mysql = obj.id
    bot_set = obj.bot_set
    return mess, id_mysql, bot_set


async def update_lastdeal(id_setings, last):
    # **************** запись в last_deal ********************
    last = int(last)
    session = Session()
    try:
        session.query(Settings).where(Settings.id_telegram == id_setings).update({Settings.last_deal: last})
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления last_deal в БД")
    session.close()


async def get_lastdeal(id_setings):
    # **************** выборка транзакций для NEXT ********************
    session = Session()
    last_deal = session.query(Settings.last_deal).where(Settings.id_telegram == id_setings).first()
    session.close()
    return last_deal


async def get_next_deal(message, id_next):
    # **************** выборка транзакций для NEXT ********************
    session = Session()
    id_setings = '2/' + str(message.from_user.id)
    next_ = session.query(Settings.wallet).where(Settings.id_telegram == id_setings).all()
    for i in range(len(next_)):
        session.query(User).filter(User.own_adress == next_[i][0]).update({User.name_token: '1'})
    obj = session.query(User).filter(User.name_token == '1').filter(User.rate == '').filter(User.bot_set != '').filter(User.id > id_next).first()
    session.close()
    return obj


async def get_next_numb(message):
    # **************** выборка транзакций для NEXT ********************
    session = Session()
    id_setings = '2/' + str(message.from_user.id)
    numb = session.query(Settings.next_).where(Settings.id_telegram == id_setings).first()
    numb = numb[0]
    return numb


async def next_mess(message, id_m):
    # **************** выборка крайней транзакции для сообщения ********************
    session = Session()
    obj = session.query(User).where(User.id == id_m).first()
    try:
        id_setings = '2/' + str(message.from_user.id)
        name_l = session.query(Settings.name_list).where(Settings.id_telegram == id_setings).first()
        print(name_l, 1, id_setings)
    except:
        name_l = session.query(Settings.name_list).where(Settings.wallet == obj.own_adress).first()
        print(name_l, 2)
    if obj.metod == "sending":
        t, o = '-', 'OUT'
    else:
        t, o = '+', 'IN'
    if obj.rate == "":
        r = "???"
    else:
        r = obj.rate
    name_list_m = (f"#{name_l[0]}")
    value_m = (f"({t}) {obj.value} usdt {o}")
    time_m = (f"{obj.time}")
    rate_m = (f"Rate: {r}")
    balance_m = (f"Balance: {obj.balance}")
    if obj.blockchain == 'trx':
        link_m1 = (f'<a href="https://tronscan.org/#/transaction/{obj.transaction_id}">LINK</a>')
    elif obj.blockchain == 'eth':
        link_m1 = (f'<a href="https://etherscan.io/tx/{obj.transaction_id}">LINK</a>')
    else:
        link_m1 = (f'<a href="https://bscscan.com/tx/{obj.transaction_id}">LINK</a>')
    id_m = (f"ID: {obj.id}")
    mess = f"{name_list_m}\n{value_m}\n{time_m}\n{rate_m}\n{balance_m}\n{link_m1} {id_m}"
    session.close()
    return mess


async def next_mess_a(id_m):
    # **************** выборка крайней транзакции для сообщения ********************
    session = Session()
    obj = session.query(User).where(User.id == id_m).first()
    name_l = session.query(Settings.name_list).where(Settings.wallet == obj.own_adress).first()
    if obj.metod == "sending":
        t, o = '-', 'OUT'
    else:
        t, o = '+', 'IN'
    if obj.rate == "":
        r = "???"
    else:
        r = obj.rate
    name_list_m = (f"#{name_l[0]}")
    value_m = (f"({t}) {obj.value} usdt {o}")
    time_m = (f"{obj.time}")
    rate_m = (f"Rate: {r}")
    balance_m = (f"Balance: {obj.balance}")
    if obj.blockchain == 'trx':
        link_m1 = (f'<a href="https://tronscan.org/#/transaction/{obj.transaction_id}">LINK</a>')
    elif obj.blockchain == 'eth':
        link_m1 = (f'<a href="https://etherscan.io/tx/{obj.transaction_id}">LINK</a>')
    else:
        link_m1 = (f'<a href="https://bscscan.com/tx/{obj.transaction_id}">LINK</a>')
    id_m = (f"ID: {obj.id}")
    mess = f"{name_list_m}\n{value_m}\n{time_m}\n{rate_m}\n{balance_m}\n{link_m1} {id_m}"
    session.close()
    return mess


async def next_update(message, n_id):
    session = Session()
    try:
        id_setings = '2/' + str(message.from_user.id)
        session.query(Settings).where(Settings.id_telegram == id_setings).update({Settings.next_: n_id})
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления next в БД")
    session.close()


async def last_up_date(message, m_id, up_id_m):
    session = Session()
    try:
        id_setings = '2/' + str(message.from_user.id)
        numb = session.query(Settings.last_deal).where(Settings.id_telegram == id_setings).first()
        if numb[0] == m_id:
            session.query(Settings).where(Settings.id_telegram == id_setings).update({Settings.last_deal: up_id_m})
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления next в БД")
    session.close()


async def cend_message_all_transactions(amount, rate, currency, purpose, metod):
    # запись в таблицу
    session = Session()
    value = amount
    name_token = 'usdt'
    balance = amount
    from_vallet = 'sending_adress'
    to_vallet = 'admission_adress'
    metod = metod
    transaction_id = "non_transaction_id"
    rate = rate
    currency = currency
    purpose = purpose
    own_adress = 'castom_adress'
    blockchain = 'usdt'
    block_timestamp = calendar.timegm(time.gmtime())
    bot_set = '2/0'
    try:
        session.add(User((f"{datetime.fromtimestamp(calendar.timegm(time.gmtime()))}+04:00"), value, name_token, balance, from_vallet, to_vallet, metod, transaction_id, rate, currency, purpose, own_adress, blockchain, block_timestamp, bot_set))
        session.commit()
    except:
        session.rollback()
        print("Ошибка добавления в БД")
    session.close()

