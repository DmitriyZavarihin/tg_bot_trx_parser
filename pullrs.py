from sql_alchemy import message_balance, get_all_transactions, update_bot_set, tg_id_get, update_rate, \
    get_id_mes_ls, get_alarm_mess, get_all_settings, get_end_tr, update_lastdeal, get_lastdeal, get_next_numb, \
    get_next_deal, next_mess, update_bot_set, next_update, last_up_date, next_mess_a
from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.exceptions import BotBlocked
from config import dp, bot
import calendar
import asyncio
import time
import re
from datetime import datetime
import pytz


async def search_new_transact():
    len_message, chanel_mes_list, block_timestamp = await get_all_transactions()
    for i in range(len_message):
        try:
            print("messege id:", block_timestamp[i].id, i)
            id_mes_ch = await bot.send_message(-1001558018797, chanel_mes_list[i], parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
            tg_id = await tg_id_get(block_timestamp[i].own_adress)
            try:
                id_mes_l = await bot.send_message(tg_id[2:], chanel_mes_list[i], parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
                tg_id = [tg_id.split('/'), []]
                tg_id[1].append(str(id_mes_ch.message_id))
                tg_id[1].append(str(id_mes_l.message_id))
                time_my = (calendar.timegm(time.gmtime()))
            except:
                tg_id = [tg_id.split('/'), []]
                tg_id[1].append('not')
                tg_id[1].append('not')
                time_my = (calendar.timegm(time.gmtime()))
            bot_set_ob = '/'.join(tg_id[0]) + "," + '/'.join(tg_id[1]) + ','+ str(time_my) + '/0/0/0'
            await update_bot_set(block_timestamp[i].id, bot_set_ob)
        except Exception as e:
            print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
            try:
                e = str(e)
                list = re.findall('(\d+)', e)
                t = int(list[0])
                await asyncio.sleep(t + 1)
            except:
                await asyncio.sleep(5)
                print(e)
            print("messege id:", block_timestamp[i].id, i)
            id_mes_ch = await bot.send_message(-1001558018797, chanel_mes_list[i], parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
            tg_id = await tg_id_get(block_timestamp[i].own_adress)
            id_mes_l = await bot.send_message(tg_id[2:], chanel_mes_list[i], parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
            tg_id = [tg_id.split('/'), []]
            tg_id[1].append(str(id_mes_ch.message_id))
            tg_id[1].append(str(id_mes_l.message_id))
            time_my = (calendar.timegm(time.gmtime()))
            bot_set_ob = '/'.join(tg_id[0]) + "," + '/'.join(tg_id[1]) + ','+ str(time_my) + '0/0/0'
            await update_bot_set(block_timestamp[i].id, bot_set_ob)
            continue


async def up_rate_message(message):
    t = message.reply_to_message.text
    ind = t.index('ID: ')
    id_msql = t[ind + 4:]
    old_mes_id = message.reply_to_message.message_id
    repley_mes_id = message.message_id
    text_mes = message.text
    try:
        text_mes = float(re.sub(r',', '.', text_mes))
        await update_rate(id_msql, text_mes)
        text = await next_mess(message, id_msql)
        await bot.edit_message_text(chat_id=-1001558018797, message_id=old_mes_id, text=text, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
        bot_set = await get_id_mes_ls(id_msql)
        await bot.edit_message_text(chat_id=bot_set[0][1], message_id=bot_set[1][1], text=text, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
        if bot_set[2][1] != '0' and bot_set[2][1] != 'dell':
                await bot.delete_message(bot_set[0][1], bot_set[2][1])
        if bot_set[2][2] != '0' and bot_set[2][2] != 'dell':
                await bot.delete_message(bot_set[0][1], bot_set[2][2])
        if bot_set[2][3] != '0' and bot_set[2][3] != 'dell':
                await bot.delete_message(bot_set[0][1], bot_set[2][3])
    except:
        pass
        print('не коректный рейт')
    await bot.delete_message(message.chat.id, repley_mes_id)


async def up_rate_flich(message):
    t = message.reply_to_message.text
    ind = t.index('ID: ')
    id_msql = t[ind + 4:]
    old_mes_id = message.reply_to_message.message_id
    repley_mes_id = message.message_id
    text_mes = message.text
    try:
        text_mes = float(re.sub(r',', '.', text_mes))
        await update_rate(id_msql, text_mes)
        print(message.from_user.id, id_msql)
        text = await next_mess(message, id_msql)
        await bot.edit_message_text(chat_id=message.from_user.id, message_id=old_mes_id, text=text, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
        bot_set = await get_id_mes_ls(id_msql)
        print(bot_set)
        await bot.edit_message_text(chat_id=-1001558018797, message_id=bot_set[1][0], text=text, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
        if bot_set[2][1] != '0' and bot_set[2][1] != 'dell':
                await bot.delete_message(bot_set[0][1], bot_set[2][1])
        if bot_set[2][2] != '0' and bot_set[2][2] != 'dell':
                await bot.delete_message(bot_set[0][1], bot_set[2][2])
        if bot_set[2][3] != '0' and bot_set[2][3] != 'dell':
                await bot.delete_message(bot_set[0][1], bot_set[2][3])
    except:
        pass
        print('не коректный рейт')
    await bot.delete_message(message.from_user.id, repley_mes_id)


async def up_rate_comment(message):
    print('up_rate_comment(message)')
    t = message.reply_to_message.text
    ind = t.index('ID: ')
    id_msql = t[ind + 4:]
    text_mes = message.text
    text_mes = float(re.sub(r',', '.', text_mes))
    await update_rate(id_msql, text_mes)
    text = await next_mess_a(id_msql)
    bot_set = await get_id_mes_ls(id_msql)
    await bot.edit_message_text(chat_id=bot_set[0][1], message_id=bot_set[1][1], text=text, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
    await bot.edit_message_text(chat_id=-1001558018797, message_id=bot_set[1][0], text=text, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
    await bot.delete_message(message.chat.id, message.message_id)
    if bot_set[2][1] != '0' and bot_set[2][1] != 'dell':
        await bot.delete_message(bot_set[0][1], bot_set[2][1])
    if bot_set[2][2] != '0' and bot_set[2][2] != 'dell':
        await bot.delete_message(bot_set[0][1], bot_set[2][2])
    if bot_set[2][3] != '0' and bot_set[2][3] != 'dell':
        await bot.delete_message(bot_set[0][1], bot_set[2][3])


async def alarm_mess():
    t = [10, 11, 12, 13, 14, 15, 16, 17, 18]
    tz = pytz.timezone("Europe/Moscow")
    current_datetime = datetime.now(tz).hour
    if current_datetime in t:
        block_timestamp = await get_alarm_mess()
        if len(block_timestamp) > 0:
            for i in range(len(block_timestamp)):
                time_my = float(calendar.timegm(time.gmtime()))
                bot_set = block_timestamp[i].bot_set
                list_rate = []
                h = bot_set.split(',')
                for j in range(3):
                    list_rate.append(h[j].split('/'))
                alarm_1 = int(list_rate[2][0])
                send_time = time_my - alarm_1
                try:
                    if send_time >= 600 and list_rate[2][1] == '0':
                        id_alarm = await bot.send_message(list_rate[0][1], "Need rate for a deal!!!", reply_to_message_id=list_rate[1][1])
                        list_rate[2][1] = str(id_alarm.message_id)
                        h = '/'.join(list_rate[0]) + "," + '/'.join(list_rate[1]) + "," + '/'.join(list_rate[2])
                        await update_bot_set(block_timestamp[i].id, h)
                    if send_time >= 3600 and list_rate[2][2] == '0':
                        await bot.delete_message(list_rate[0][1], list_rate[2][1])
                        id_alarm = await bot.send_message(list_rate[0][1], "Need rate for a deal!!!", reply_to_message_id=list_rate[1][1])
                        list_rate[2][2] = str(id_alarm.message_id)
                        list_rate[2][1] = 'dell'
                        h = '/'.join(list_rate[0]) + "," + '/'.join(list_rate[1]) + "," + '/'.join(list_rate[2])
                        await update_bot_set(block_timestamp[i].id, h)
                    if send_time >= 10620 and list_rate[2][3] == '0':
                        await bot.delete_message(list_rate[0][1], list_rate[2][2])
                        id_alarm = await bot.send_message(list_rate[0][1], "Need rate for a deal!!!", reply_to_message_id=list_rate[1][1])
                        list_rate[2][3] = str(id_alarm.message_id)
                        list_rate[2][2] = 'dell'
                        h = '/'.join(list_rate[0]) + "," + '/'.join(list_rate[1]) + "," + '/'.join(list_rate[2])
                        await update_bot_set(block_timestamp[i].id, h)
                except:
                    print("Error alarm mess")
    else:
        print("time wrong")


async def last_deal(message):
    id_setings = '2/' + str(message.from_user.id)
    settings = await get_all_settings(id_setings)
    mess = ['WHAT WALLET?\n']
    for i in range(len(settings)):
        mess.append(f"/{settings[i].name_list}-{settings[i].group}\n")
    mess_str = ''.join(mess)
    m = await bot.send_message(message.from_user.id, mess_str)
    await update_lastdeal(id_setings, m.message_id)


async def last_mesagge(message):
    id_setings = '2/' + str(message.from_user.id)
    last_deal = await get_lastdeal(id_setings)
    try:
        await bot.delete_message(message.from_user.id, last_deal[0])
    except:
        print("no dell message")
    try:
        mess, id_mysql, bot_set = await get_end_tr(message.text)
        id_mes_ch = await bot.send_message(message.from_user.id, mess, parse_mode=types.ParseMode.HTML,
                                           disable_web_page_preview=True)
        if bot_set != '':
            list_rate = []
            h = bot_set.split(',')
            for i in range(3):
                list_rate.append(h[i].split('/'))
            await bot.delete_message(message.from_user.id, list_rate[1][1])
            list_rate[1][1] = str(id_mes_ch.message_id)
            h = '/'.join(list_rate[0]) + "," + '/'.join(list_rate[1]) + "," + '/'.join(list_rate[2])
            await update_bot_set(id_mysql, h)
        else:
            print('rate пуст')
    except:
        await bot.send_message(message.from_user.id, f'{message.text[1:]} - no deals with USDT!!!')


async def next_mess_up(message):
    numb = await get_next_numb(message)
    obj = await get_next_deal(message, numb)
    if obj != None:
        bot_set = await get_id_mes_ls(obj.id)
        try:
            await bot.delete_message(message.chat.id, bot_set[1][1])
        except:
            print("no dell message")
        mess = await next_mess(message, obj.id)
        id_mes_l = await bot.send_message(message.chat.id, mess, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
        await next_update(message, obj.id)
        await last_up_date(message, bot_set[1][1], id_mes_l.message_id)
        bot_set[1][1] = str(id_mes_l.message_id)
        h = '/'.join(bot_set[0]) + "," + '/'.join(bot_set[1]) + "," + '/'.join(bot_set[2])
        await update_bot_set(obj.id, h)
        print(id_mes_l.message_id)
    if obj == None and numb == 0:
        await bot.send_message(message.chat.id, "No messages this wallet!!!")
    if obj == None and numb > 0:
        await next_update(message, 0)
        await bot.send_message(message.chat.id, 'Click next again to start from the first messages!!!')
