from sql_alchemy import message_balance, get_all_transactions, update_bot_set, tg_id_get, update_rate, get_id_mes_ls,\
    get_end_tr, cend_message_all_transactions
from pullrs import search_new_transact, up_rate_message, up_rate_flich, up_rate_comment, alarm_mess, last_mesagge, \
    last_deal, next_mess_up
from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.exceptions import BotBlocked
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from asyncio import get_event_loop
from menu import menu_client
from add import add_client
from purpose import menu_purpose
from in_out import menu_inout
from config import dp, bot
import asyncio
import time
import re
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.fsm_storage.memory import MemoryStorage


class FSMadd(StatesGroup):
    amount = State()
    rate = State()
    currency = State()
    purpose = State()
    metod = State()


@dp.message_handler(commands=['start', 'help'])
async def comand_start(message: types.message):
    await bot.send_message(message.from_user.id, "Welcome", reply_markup=menu_client)
    await message.delete()


@dp.message_handler(regexp='BALANCE')
async def comand_balance(message: types.message):
    messag = await message_balance()
    await bot.send_message(message.from_user.id, messag, parse_mode=types.ParseMode.HTML)


@dp.message_handler(regexp='LAST DEAL')
async def comand_last(message: types.message):
    await last_deal(message)


@dp.message_handler(regexp='ADD')
async def comand_add(message: types.message):
    await FSMadd.amount.set()
    await message.answer('Amount?')


@dp.message_handler(state=FSMadd.amount)
async def load_amout(message: types.message, state: FSMContext):
    async with state.proxy() as data:
        data['amount'] = message.text
    await FSMadd.rate.set()
    await message.answer('Rate?')


@dp.message_handler(state=FSMadd.rate)
async def load_amout(message: types.message, state: FSMContext):
    async with state.proxy() as data:
        data['rate'] = message.text
    await FSMadd.currency.set()
    await message.answer('Currency?')

@dp.message_handler(state=FSMadd.currency)
async def load_rate(message: types.message, state: FSMContext):
    async with state.proxy() as data:
        data['currency'] = message.text
    await FSMadd.purpose.set()
    await bot.send_message(message.from_user.id, "Purpose?", reply_markup=menu_purpose)


@dp.message_handler(state=FSMadd.purpose)
async def load_purpose(message: types.message, state: FSMContext):
    async with state.proxy() as data:
        data['purpose'] = message.text
    await FSMadd.metod.set()
    await bot.send_message(message.from_user.id, "IN or OUT?", reply_markup=menu_inout)


@dp.message_handler(state=FSMadd.metod)
async def load_metod(message: types.message, state: FSMContext):
    async with state.proxy() as data:
        data['metod'] = message.text
    pur = ["CSH", "CHQ", "DEAL", "COST", "INCM"]
    in_out = ["admission", "sending"]
    amount = data['amount']
    rate = data['rate']
    currency = data['currency']
    purpose = data['purpose']
    metod = data['metod']
    try:
        amount = float(re.sub(r',', '.', amount))
        rate = float(re.sub(r',', '.', rate))
        if purpose in pur and metod in in_out:
            print(f"amount: {amount}, rate: {rate}, currency: {currency}, purpose: {purpose}, metod: {metod}")
            await cend_message_all_transactions(amount, rate, currency, purpose, metod)
            await bot.send_message(message.from_user.id, f"Success!!! amount: {amount}, rate: {rate}, currency: {currency}, purpose: {purpose}, metod: {metod}")
    except Exception as e:
        print(e)
        await bot.send_message(message.from_user.id, "Input Error, please try again!!!")
    await bot.send_message(message.from_user.id, "Welcome", reply_markup=menu_client)
    await state.finish()


@dp.message_handler(regexp='NEXT')
async def comand_next(message: types.message):
    await next_mess_up(message)


@dp.message_handler(regexp='BACK')
async def comand_back(message: types.message):
    await bot.send_message(message.from_user.id, "Welcome", reply_markup=menu_client)
    await message.delete()


@dp.message_handler(regexp='MENU')
async def comand_menu(message: types.message):
    await bot.send_message(message.from_user.id, "Welcome", reply_markup=menu_client)
    await message.delete()


@dp.message_handler()
async def echo_send(message: types.message):
    if message.reply_to_message and message.reply_to_message.text[0] == "#" and message.chat.id != -1001844048814:
        await up_rate_flich(message)
    elif message.reply_to_message and message.reply_to_message.text[0] == "#" and message.chat.id == -1001844048814:
        await up_rate_comment(message)
    else:
        if message.text[0] == '/':
            await message.delete()
            await last_mesagge(message)
        else:
            await bot.delete_message(message.chat.id, message.message_id)


@dp.channel_post_handler()
async def rate_repley(message: types.Message):
    if message.reply_to_message and message.reply_to_message.text[0] == "#":
        await up_rate_message(message)
    else:
        await bot.delete_message(message.chat.id, message.message_id)


async def notify_message():
    while True:
        await asyncio.sleep(10)
        await search_new_transact()
        await alarm_mess()



async def on_startup(dp):
    asyncio.create_task(notify_message())


if __name__ == '__main__':
    executor.start_polling(dp, on_startup=on_startup, skip_updates=True)

